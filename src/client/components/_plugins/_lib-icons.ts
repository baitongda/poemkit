
/*
  Fontawesome
  ------------- 
*/
import '@poemkit/components/_plugins/FontAwesome/scss/fontawesome.scss';  
import '@poemkit/components/_plugins/FontAwesome/scss/v4-shims.scss';  
import '@poemkit/components/_plugins/FontAwesome/scss/solid.scss';  
import '@poemkit/components/_plugins/FontAwesome/scss/brands.scss';  
import '@poemkit/components/_plugins/FontAwesome/scss/regular.scss';  

