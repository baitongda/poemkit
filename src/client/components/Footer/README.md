# @poemkit/components/Footer

[Source](https://github.com/xizon/poemkit/tree/main/src/client/components/Footer)

## Version

=> 0.0.1 (October 10, 2021)

## API

### Footer
```js
import Footer from '@poemkit/components/Footer/index.tsx';
```
| Property | Type | Default | Description |
| --- | --- | --- | --- |
| - | - | - | - |


## Examples

```js
import React from 'react';
import Footer from '@poemkit/components/Footer/index.tsx';

export default () => {
  return (
    <>
		<Footer />
    </>
  );
}

```